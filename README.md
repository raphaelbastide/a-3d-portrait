# A 3D Portrait

Tool used:
- SculptGl https://stephaneginier.com/sculptgl/
- Three.js library

# Sculpting

1. https://stephaneginier.com/sculptgl/

# Scene making

1. Export .obj from sculptgl
2. Export Diffuse from sculptgl
3. Download the template
4. Paste the .obj and the diffuse.png in the folder
5. Rename the .obj to bust.obj, and the diffuse.png to texture.png
6. Also make sure to modify the map.jpg with a equirectangular map image

## Sound

- must loop
- must be made by you
- must be as a html5 player

## Ressources

- https://threejs.org/ and https://threejs.org/examples/
- https://threejsfundamentals.org/
